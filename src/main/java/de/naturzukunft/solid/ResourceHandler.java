package de.naturzukunft.solid;

import java.util.List;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

public class ResourceHandler {

	public void handle(int tab, Resource resource) {
		StmtIterator properties = resource.listProperties();
		List<Statement> list = properties.toList();
		if(list.isEmpty()) {
			log(tab, resource.toString());
		}
		for (Statement stmt : list) {
			if(stmt.getObject().isStmtResource()) {
				log(tab, "STATEMENT: " + stmt.getObject().toString());				
			} else if(stmt.getObject().isURIResource()) {				
				Resource r2 = stmt.getObject().asResource();
				if( r2.listProperties().toList().isEmpty() ) {
				}
				else {			
					log(tab, "ref:" + stmt.getPredicate().toString());
					handle(tab + 2, r2);
				}
			} 
			else if(stmt.getObject().isLiteral()) {
				log(tab, stmt.getObject().toString() + " ["+stmt.getPredicate()+"]");
			}
		}
	}
	
	public static void log(int tabs, String msg) {
		String tab = "";
		for (int i = 0; i < tabs; i++) {
			tab+=" ";
		}
		System.out.println(tab + msg);
	}
}
