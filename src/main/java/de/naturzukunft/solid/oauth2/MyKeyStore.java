package de.naturzukunft.solid.oauth2;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class MyKeyStore {

	private Map<String, KeyPair> cache = new HashMap<>();
	
	public KeyPair getKeyPair(String userName) {
		if( !this.cache.containsKey(userName) ) 
		{
			this.cache.put(userName, createRSA256KeyPair());
		}
		return cache.get(userName);
	}
	
	private KeyPair createRSA256KeyPair() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(2048);
			return generator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}	
}
