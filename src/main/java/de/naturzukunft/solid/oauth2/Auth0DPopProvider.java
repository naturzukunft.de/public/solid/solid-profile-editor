package de.naturzukunft.solid.oauth2;

import java.net.URI;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Auth0DPopProvider implements DPopProvider {

	private static final MyKeyStore myKeyStore = new MyKeyStore();
	private static Map<String, Builder> cache = new HashMap<>();
	
//	public static void main(String[] args ) {
//		Auth0DPopProvider a = new Auth0DPopProvider(new MyKeyStore());
//		String token = a.getDPopToken("userName", HttpMethod.POST, URI.create("http://test"));
//		
//		try {
//		    DecodedJWT jwt = JWT.decode(token);
//		    System.out.println("Header");
//		    System.out.println(StringUtils.newStringUtf8(Base64.decodeBase64(jwt.getHeader())));
//		    System.out.println("Payload");
//		    System.out.println(StringUtils.newStringUtf8(Base64.decodeBase64(jwt.getPayload())));
//		} catch (JWTDecodeException exception){
//		    //Invalid token
//		}
//		
//	}
	
	@Override
	public String getDPopToken(String userName, HttpMethod httpMethod, URI uri) {

		String token = "";
		try {
			token = getCachedJWTCreatorBuilder(userName)
			.withClaim("htm", httpMethod.name())
			.withClaim("htu", uri.toString())
			.sign(getAlgorithm(userName));
		} catch (JWTCreationException exception) {
			// Invalid Signing configuration / Couldn't convert Claims.
			throw new RuntimeException("Invalid Signing configuration / Couldn't convert Claims");
		}
		return token;
	}
	
	private Algorithm getAlgorithm(String userName) {
		KeyPair keyPair = myKeyStore.getKeyPair(userName);
		return Algorithm.RSA256((RSAPublicKey)keyPair.getPublic(), (RSAPrivateKey) keyPair.getPrivate());
	}
	
	private Builder getJWTCreatorBuilder(String userName) {
		
		Map<String, Object> headerClaims = new HashMap<>();
		headerClaims.put("typ", "dpop+jwt");
		headerClaims.put("alg", "RS256");
		headerClaims.put("jwk", generateJWK(myKeyStore.getKeyPair(userName).getPublic()));

		JWTCreator.Builder builder = JWT.create()
				.withHeader(headerClaims)
				.withClaim("jti", UUID.randomUUID().toString())
				.withClaim("iat", Date.from(Instant.now()));
		return builder;
	}
	
	private Map<String, Object> generateJWK(PublicKey publicKey){
	    RSAPublicKey rsa = (RSAPublicKey) publicKey;
	    Map<String, Object> values = new HashMap<>();
	    values.put("kty", rsa.getAlgorithm()); // getAlgorithm() returns kty not algorithm
	    values.put("n", java.util.Base64.getUrlEncoder().encodeToString(rsa.getModulus().toByteArray()));
	    values.put("e", java.util.Base64.getUrlEncoder().encodeToString(rsa.getPublicExponent().toByteArray()));
	    values.put("alg", "RS256");
	    return values;
	}

	private Builder getCachedJWTCreatorBuilder(String userName) {
		if( !cache.containsKey(userName) ) {
			cache.put(userName, getJWTCreatorBuilder(userName));
		}
		return cache.get(userName);
	}
}
